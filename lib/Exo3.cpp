#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int i = 0;
int d = 1;
int a;

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void ChangePalettePeriodically()
{
    a = a + 25;
    leds[i] = CHSV(a, 255, 255);
}

void loop()
{

    FastLED.clear();
    ChangePalettePeriodically();
    FastLED.show();
    FastLED.clear();
    delay(100);
    i = i + d;
    if (NUM_LEDS-2 == i - 1 || i == 0)
    {
        d = d * -1;
    }
}
#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int numLedPrinc = 0;
int coul = 0;
int i;

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
void loop()
{

    leds[i % NUM_LEDS] = CHSV(coul, 255, 255);
    FastLED.show();
    delay(10); //delais de 300
    coul = coul + 25.5;
    i++;
    if (coul > 255)
    {
        coul = 0;
    }
}

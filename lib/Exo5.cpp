#include <Arduino.h>
//Pour adapter la lib FastLED à l'ESP
#define FASTLED_ESP8266_RAW_PIN_ORDER
//ajout de la lib
#include <FastLED.h>
#define NUM_LEDS 10 //nombre de LED du ruban
#define DATA_PIN 2  //D4 broche du bus de commande du ruban
//Création d'un tableau pour stocker et contrôler les LEDS
CRGB leds[NUM_LEDS];

int numLedPrinc = 0;
float coul = 0;

void allumerLeds(int prmNumPrincipal)
{
    FastLED.clear(); //supprimer l'affichage des leds
    prmNumPrincipal = prmNumPrincipal + numLedPrinc;

    leds[((prmNumPrincipal - 1 + NUM_LEDS) % NUM_LEDS)] = CHSV(coul, 150, 255); //affichage led gauche
    leds[prmNumPrincipal % NUM_LEDS] = CHSV(coul, 255, 255);                    //affichage led principale
    leds[(prmNumPrincipal + 1) % NUM_LEDS] = CHSV(coul, 150, 255);              //affigage led droite
    FastLED.show();                                                             //afficher les leds
    numLedPrinc++;                                                              //numero de la led principale +1
    if (numLedPrinc > NUM_LEDS)
    {
        coul = coul + 25.5;
        numLedPrinc = 1;
    }
}

void setup()
{
    //configuration du ruban de LEDs
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}
void loop()
{
    allumerLeds(0); //led principal initialisé a la case 3
    delay(300);     //delais de 300
}
